import { Component } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'version-dialog',
  templateUrl: './version-dialog.component.html',
  styleUrls: ['./version-dialog.component.css'],
})
export class VersionDialogComponent {
  version!: string | null;

  constructor(
    public dialog: MatDialog
    ) {
    this.version = '0.0.0'
  }

  getVersion() {
    return '0.0.0';
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogContentExampleDialog);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}

@Component({
  selector: 'dialog-content',
  templateUrl: 'dialog-content.html',
})
export class DialogContentExampleDialog {}
