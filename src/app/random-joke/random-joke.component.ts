import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'random-joke',
  templateUrl: './random-joke.component.html',
  styleUrls: ['./random-joke.component.css'],
})
export class RandomJokeComponent {
  joke!: string | null;

  constructor(private http: HttpClient) {}

  getJoke() {
    let body: string;
    this.http.get<any>('https://api.chucknorris.io/jokes/random').subscribe(
      result => this.joke = result.value
    );
  }

  remove() {
    this.joke = null;
  }
}
